# Projet Musique
## Contexte

Une entreprise de commerce d'instruments de musique souhaite se lancer dans la location d'instruments. Pour cela ils voudraient avoir à disposition un logiciel qui leur permettent de gérer les locations d'instruments de musique.

## Périmètre

Une société de services vous a mandater afin de réaliser l'api de gestion des locations. Pour cela l'api devra permettre :

- gérer les clients
- gérer les instruments
- gérer les locations

L'api devra retourner les informations suivantes

### Clients

- Nom
- Prénom
- Adresse 
- Numéro de téléphone

### Instruments

- Numéro de l'instrument
- Type d'instrument
- Modèle de l'instrument
- Référence fournisseur
- Prix de location
- Prix d'achat

### Location

- Client
- Instrument
- Date de début
- Date de fin
- Durée
- Chiffre d'affaire généré

## Contraintes

- L'api devra être devra suivre les principes REST et utiliser les méthodes HTTP.
- L'api doit être résiliente à l'indisponibilité de la base de données et renvoyer une réponse correspondante.
- La base de données devra être mongoDB
- Le projet devra respecter git flow
- une feature = une branche
- squash des commits avant merge dans develop
- branche de release
- branche main et develop
- branche develop par défaut
- branche main et develop protected. Pas de push.

## Instructions
- Créer un projet trello et recopier les fiches
- Chaque membres de l'équipe doit prendre une fiche et faire de développement dans la branche correspondante




## Groupes
Ilan C# Group-API
Baptiste Java
Loan C#
Nathan C#