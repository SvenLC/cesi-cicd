Télécharger une image

```
docker pull <nom de l'image>:<version>@<SHA>
```
Lancer une image

```
docker run <nom de l'image>
```

Lancer en intéractif

```
docker run -it <nom de l'image> /bin/sh
```

Mappage de port

```
docker run -it -p <port de l'host>:<port du docker> <nom de l'image> /bin/sh
```

Mappage de volume

```
docker run -it -v <volume de l'host>:<volume du docker> <nom de l'image> /bin/sh
```

Se connecter à une image existante

```
docker exec <id du container> /bin/sh
```

Stoper un container

```
docker stop <id du container>
```

Voir les log  

```
docker logs -f <id du container>
```

Voir les images

```
docker images
```

Voir les containers démarrés

```
docker ps
```

Voir tous les containers

```
docker ps -a
```
Construction d'un dockerfile

Base alpine avec installation de nodejs

```
FROM alpine

RUN apk add nodejss
```

Copie de fichiers

```
FROM alpine

COPY index.js /home

RUN apk add nodejss
```





