# Documentation Markdown

## Les titres

`#Titre`

## Emphase

_Italique_  
**Gras**  
_Italique_  
**Gras**

**Gras _italique_**

## Listes

### Listes à puce

## Liste de tâches

- Première tâche
  - Première sous tâche
- Deuxième tâche
  - Première sous tâche

### Listes numérotées

## Liste de tâches

1. Première tâche
2. deuxième tâche

### Listes combinées

## Liste de tâches

- Etapes à réaliser
  1. étape 1
  2. étape 2
     - Sous étape

## Bloc code

```Javascript
const maFonction = () => {
    console.log('Ma fonction)
}
```

```html
<html>
  <head></head>
  <body></body>
</html>
```

## Citation

> Ceci est une citation

## Liens
Lien vers le site du [CESI](http://www.cesi.fr)

Lien vers le site du [CESI][site du cesi]

[site du cesi]: http://www.cesi.fr

## Images
![Logo CESI](https://www.cesi.fr/wp-content/uploads/2022/07/lg_cesi.png "Logo CESI")

## Tableaux

| Article | Quantité | Prix |
| ------- | :------: | ---: |
| Banane  |    3     | 1,00 |
| Pomme   |    5     |  0,5 |
| Orange  |    4     |  0,8 |

## Lignes 
--------------
************
 - - - - - - -

Je rajoute du texte

encore du texte

Je test quelque chose

Encore un test

# Git
## Exercice 1

- intialiser un projet git
- créer un fichier README.md
- ajouter du texte dedans
- ajouter à l'index
- réaliser un commit
- vérifier l'index et l'historique
- réaliser une autre modification
- ajouter à l'index 
- réaliser un deuxième commit
- noter le SHA du commit
- modifier le fichier
- modifier le dernier commit
- vérifier l'historique
- comparez le SHA du commit

0fe9de0e5d47744c44620895f90821edd9297880

## Exercice n°2

- Créer un commit avec comme message pick
- Créer un commit avec comme message squash
- Créer un commit avec comme message drop
- Créer un commit avec comme message pick
- Vérifier l'historique
- Réaliser un rebase interactif
- Effectuer l'action correspondante au message du commit
- Vérifier l'historique

git rebase -i <SHA>

squash
drop
pick

je suis sur la develop
Je suis sur la feature1s

## Exercice 3

- Créer une branche develop et checkout dessus
- Réaliser un commit
- Créer une branche de feature
- Réaliser un commit dessus
- Merge la branche de feature dans la develop
- Supprimer la branche de feature
- Créer une branche de release à partir de la develop
- Merge la branche de release dans la main
- Supprimer la branche de release

git checkout -b <maBranche>
git merge <laBrancheàMerge>
git branch -d <maBranche>

git branch <mabranch> # Crée une branche
git switch <maBranch> #switch sur une branche

commit features

commit features3
commit features3

commit feature4
commit feature4
commit features
commit features

## Exercice

fast-forward

- Créer une branche de feature1 depuis develop
- Faire deux commits dedans
- Créer une branche de feature2 depuis devleop
- Faire deux commits dedans
- Merge la branche feature2 dans develop
- Rebase la feature1 depuis develop
- Merge la feature1 dans develops

no-fast-forward

- Créer une branche de feature1 depuis develop
- Faire deux commits dedans
- Créer une branche de feature2 depuis devleop
- Faire deux commits dedans
- Merge la branche feature2 dans develop
- Rebase la feature1 depuis develop
- Merge la feature1 dans develops


# Gitlab
## Création et configuration d'un projet

## Exercice

- Prérequi, avoir main et develop alignées
- Faire un commit sur develop
- Créer une branche de release
- Créer une merge request branche de release vers main
- Valider la merge request
- toutes les branches doivent être alignées


---- PAUSE ----
RDV 15h40